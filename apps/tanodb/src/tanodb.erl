-module(tanodb).

-export([ping/0, get/2, get/3, put/3, put/4,  delete/2, delete/3, keys/1,
		 keys/2]).
-ignore_xref([ping/0, get/2, get/3, put/3, put/4,  delete/2, delete/3, keys/1,
			  keys/2]).

-define(N, 3).
-define(W, 3).
-define(TIMEOUT, 5000).

%% Public API

%% @doc Pings a random vnode to make sure communication is functional
ping() ->
    send_to_one(<<"ping">>, term_to_binary(os:timestamp()), ping).

get(Bucket, Key) ->
	get(Bucket, Key, #{}).

get(Bucket, Key, Opts) ->
	K = {Bucket, Key},
	Params = K,
	run_quorum(get, K, Params, Opts).

put(Bucket, Key, Value) ->
	put(Bucket, Key, Value, #{}).

put(Bucket, Key, Value, Opts) ->
	K = {Bucket, Key},
	Params = {Bucket, Key, Value},
	run_quorum(put, K, Params, Opts).

delete(Bucket, Key) ->
	delete(Bucket, Key, #{}).

delete(Bucket, Key, Opts) ->
	K = {Bucket, Key},
	Params = K,
	run_quorum(delete, K, Params, Opts).

keys(Bucket) ->
	keys(Bucket, #{}).

keys(Bucket, Opts) ->
	Timeout = maps:get(timeout, Opts, ?TIMEOUT),
    tanodb_coverage_fsm:start({keys, Bucket}, Timeout).

%% Private Functions

run_quorum(Action, K, Params, Opts) ->
	N = maps:get(n, Opts, ?N),
	W = maps:get(w, Opts, ?W),
	Timeout = maps:get(timeout, Opts, ?TIMEOUT),
    ReqId = make_ref(),
	tanodb_write_fsm:run(Action, K, Params, N, W, self(), ReqId),
	wait_for_reqid(ReqId, Timeout).

wait_for_reqid(ReqId, Timeout) ->
    receive
        {ReqId, Val} -> Val
    after
        Timeout -> {error, timeout}
    end.

send_to_one(Bucket, Key, Cmd) ->
    DocIdx = riak_core_util:chash_key({Bucket, Key}),
    PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, tanodb),
    [{IndexNode, _Type}] = PrefList,
    riak_core_vnode_master:sync_spawn_command(IndexNode, Cmd, tanodb_vnode_master).

